# ApacheSmartQueries

## Getting Started
Apache-Smart-Queries is a Desktop application for downloading directories and files from an existing Apache Server.

## Installation
Execute the following command to set up the virtual environment for running the main.py using python.
```
pip install -e .[tests] 
```
In case you want to produce a new executable file, please run the following command inside the venv terminal:
```
pyinstaller src/ApacheSmartQueries/main.py --onefile -w -F --icon=./src/ApacheSmartQueries/icon/ASQ_Logo.ico
```

## Dependencies
* python >= 3.10
* requests==2.31.0
* beautifulsoup4==4.12.2
* pyinstaller==6.2.0

## Description of Execution Process
For the execution of the application run either the *.exe file in Windows systems or the main.py file.
Figure 1 illustrates a preview of the main window, which is divided to three parts. The first part is
used for accessing the Apache server, the second part is used for searching the database, and the last
part is using for viewing and selecting the data.

|                                         ![MainWindow_Indexing.png](img_readme/MainWindow_Indexing.png)                                          |
|:-----------------------------------------------------------------------------------------------------------------------------------------------:| 
| *Figure 1: Preview of the Main Window: (1) Enter the URL of the Apache Server; (2) Filter for smart query searching; <br/>(3) Database preview* |

Figure 2 illustrates and example of using the application. Note that the Apache Server address is masked out for
security reason. The application is also supporting utf-8 text formation, thus it can be used for several languages.
In this example, the search filter ask to find all database entries of pdf type. The selected entry with id=219 can
be easily downloaded by pressing the "Download" button. All downloaded files can be found in the Download folder
(in the current version it cannot be changed).

|           ![SearchExample.png](img_readme/SearchExample.png)            |
|:-----------------------------------------------------------------------:| 
| *Figure 2: A Search Example using the Apache-Smart-Queries application* |

It is also, worth mentioning, that when the "Search" button is pressed, two possible pop up windows can be viewed.
The first one is an information message, which informs the user for the successful query generation and the connection
to the server, while the second one is a critical fail error indicating that the path didn't be found or failures 
between the server-client connection. Similarly, when downloading an entry, an information pop up is used to inform
the user after a successful entry downloading, while a warning message informs the user that something occurred during
the download process (i.e., connection failure) and the downloaded file may be corrupted (i.e., not working). Usually,
failures can be seen during downloading large files, but it is also possible for physical connection failures during
downloading directory entries (in this case, the downloaded files are working).

## License
This project is licensed under the GNU General Public License v3.0 or later.

## Project status
Currently, this project is finished. Maybe in the future more features will be added, if necessary. This version
is also free of bugs.
