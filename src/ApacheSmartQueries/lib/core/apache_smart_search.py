import os
import requests
from bs4 import BeautifulSoup
import urllib.parse

from ApacheSmartQueries.lib.core.queries import *

_SUPPORT_EXT = ['/', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'kml', 'txt', 'zip', 'gdb']


class ApacheSmartSearch:
    def __init__(self, url: str):
        self.url = url
        self.queries = []
        self.downloadFolder = self.get_download_path()

    @staticmethod
    def get_download_path():
        """Returns the default downloads path for linux or windows"""
        if os.name == 'nt':
            import winreg
            sub_key = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders'
            downloads_guid = '{374DE290-123F-4565-9164-39C4925E467B}'
            with winreg.OpenKey(winreg.HKEY_CURRENT_USER, sub_key) as key:
                location = winreg.QueryValueEx(key, downloads_guid)[0]
            return location
        else:
            return os.path.join(os.path.expanduser('~'), 'downloads')

    @staticmethod
    def _get_url_paths(url, ext: list = None):
        if ext is None:
            ext = _SUPPORT_EXT
        url_list = []
        try:
            r = requests.get(url)
            if r.ok:
                soup = BeautifulSoup(r.text, 'html.parser')
                for node in soup.find_all('a'):
                    for fmt in ext:
                        if node.get('href').endswith(fmt):
                            path = url + node.get('href')
                            if not path.split('http://')[1].__contains__('//'):
                                url_list.append(path)
                            break
        except:
            pass

        return url_list

    def _download_directory(self, url, save_path):
        # Create the save path if it doesn't exist
        if not os.path.exists(save_path):
            os.makedirs(save_path)

        l_url = self._get_url_paths(url)
        for link in l_url:
            item = urllib.parse.unquote(link.split(url)[-1])
            if item.split('.').__len__() > 1:
                with open(os.path.normpath(save_path + "/" + item), 'wb') as f:
                    f.write(requests.get(link).content)
            else:
                self._download_directory(link, save_path + "/" + item)

    def exist_url(self):
        if self.url:
            r = requests.get(self.url)
            return r.status_code == 200, r.status_code
        else:
            return False, 404

    def _add_url(self, url):
        url_list = self._get_url_paths(url)
        for __url__ in url_list:
            if __url__.endswith('/'):
                query = Q_APACHE.copy()
                query['id'] = self.queries.__len__()
                dir_name = os.path.dirname(__url__).split(self.url)[1].split('/')
                query['name'] = urllib.parse.unquote(os.path.basename(dir_name[-1]))
                query['type'] = 'directory'
                query['in_folder'] = []
                if dir_name.__len__() - 1 <= 0:
                    query['in_folder'].append("root")
                else:
                    for __index__ in range(0, dir_name.__len__() - 1):
                        query['in_folder'].append(urllib.parse.unquote(dir_name[__index__]))
                query['url'] = __url__

                self.queries.append(query)
                self._add_url(query['url'])

            else:
                query = Q_APACHE.copy()
                query['id'] = self.queries.__len__()
                query['name'] = urllib.parse.unquote(os.path.basename(__url__))
                query['type'] = 'file'
                if os.path.basename(__url__).split('.').__len__() > 1:
                    query['type'] = urllib.parse.unquote(os.path.basename(__url__).split('.')[-1])
                query['in_folder'] = []
                dir_name = os.path.dirname(__url__).split(self.url)[1].split('/')
                for __dir__ in dir_name:
                    query['in_folder'].append(urllib.parse.unquote(__dir__))
                query['url'] = __url__

                self.queries.append(query)

    def generate_queries(self):
        self._add_url(self.url)

    def get_queries(self, qfilter=None, f_type='name'):
        if qfilter is None:
            return self.queries
        else:
            queries = []
            if f_type == 'in_folder':
                for query in self.queries:
                    if query['in_folder'][-1].__contains__(qfilter):
                        queries.append(query)
            else:
                for query in self.queries:
                    if query[f_type].__contains__(qfilter):
                        queries.append(query)
            return queries

    def download(self, item):
        try:
            item_id = item[0]
            for query in self.queries:
                if item_id == query['id']:
                    if item[2] != 'directory':
                        response = requests.get(query['url'])
                        with open(os.path.normpath(self.downloadFolder + "/" + query['name']), 'wb') as f:
                            f.write(response.content)
                    else:
                        self._download_directory(query['url'],
                                                 os.path.normpath(self.downloadFolder + "/" + query['name']))
            return True
        except:
            return False
