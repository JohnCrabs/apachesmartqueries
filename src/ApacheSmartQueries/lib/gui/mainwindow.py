import tkinter as tk
from tkinter import ttk, messagebox
import ApacheSmartQueries.lib.core.apache_smart_search as ass


class ApacheSmartQueriesGUI:
    def __init__(self):
        # Initialize Apache Queries
        self.apache = None

        # Create the main window
        self.rWin = tk.Tk()
        self.setRootWin(
            title="Apache Smart Queries",
            resizable=False,
            iconPath="./icon/ASQ_Logo.png"
        )
        # ------ START BUILDING THE APP ------
        # Variable
        self.textKeywordSearch = tk.StringVar()
        self.textSearchBy = tk.StringVar()
        self.url = tk.StringVar()

        # Apache Server
        lf_ApacheServer = tk.LabelFrame(self.rWin, text="Apache Server:", padx=5, pady=5)
        tk.LabelFrame(lf_ApacheServer, text="Domain: ").grid(row=0, column=0)
        entry_Domain = tk.Entry(lf_ApacheServer, show='*', textvariable=self.url, width=93)
        entry_Domain.grid(row=0, column=1)
        button_Connect = tk.Button(lf_ApacheServer, text="Connect", command=self.initialize_apache)
        button_Connect.grid(row=0, column=2)

        lf_ApacheServer.grid(row=0, column=0)

        # Search Bar
        lf_SearchBar = tk.LabelFrame(self.rWin, text="Search Bar:", padx=5, pady=5)

        # Search Filter
        lf_SearchFilter = tk.LabelFrame(lf_SearchBar, text="Search By:", padx=5)
        self.radio_Name = tk.Radiobutton(lf_SearchFilter, text="Name", variable=self.textSearchBy, value="name")
        self.radio_Name.select()
        self.radio_Name.grid(row=0, column=0, sticky=tk.E)
        self.radio_Type = tk.Radiobutton(lf_SearchFilter, text="Type", variable=self.textSearchBy, value="type")
        self.radio_Type.grid(row=0, column=1, sticky=tk.E)
        self.radio_Folder = tk.Radiobutton(lf_SearchFilter, text="Folder", variable=self.textSearchBy, value="in_folder")
        self.radio_Folder.grid(row=0, column=2, sticky=tk.E)
        lf_SearchFilter.grid(row=0, column=0)

        tk.LabelFrame(lf_SearchBar, text="", width=5).grid(row=0, column=1)
        entry_Keyword = tk.Entry(lf_SearchBar, textvariable=self.textKeywordSearch, width=50)
        entry_Keyword.grid(row=0, column=2)
        tk.LabelFrame(lf_SearchBar, text="", width=5).grid(row=0, column=3)
        button_Search = tk.Button(lf_SearchBar, text="Search", command=self.action_Search)
        button_Search.grid(row=0, column=4)
        tk.LabelFrame(lf_SearchBar, text="", width=5).grid(row=0, column=5)
        button_Download = tk.Button(lf_SearchBar, text="Download", command=self.action_Download)
        button_Download.grid(row=0, column=6)

        lf_SearchBar.grid(row=1, column=0)

        # Tree Structure
        lf_TreeStructure = tk.LabelFrame(self.rWin, text="Browse (Folders/Files):", padx=5, pady=5)
        self.trv = ttk.Treeview(lf_TreeStructure, selectmode="browse", height=10)
        self.trv['columns'] = ("1", "2", "3", "4")
        self.trv['show'] = "headings"
        self.trv.column("1", width=80, anchor='c')
        self.trv.column("2", width=180, anchor='c')
        self.trv.column("3", width=180, anchor='c')
        self.trv.column("4", width=180, anchor='c')
        self.trv.heading("1", text='id')
        self.trv.heading("2", text='Name')
        self.trv.heading("3", text='Type')
        self.trv.heading("4", text='Folder')
        self.trv.grid(row=0, column=0, sticky=tk.W+tk.E)
        lf_TreeStructure.grid(row=2, column=0)

        tk.LabelFrame(self.rWin, text="", height=5).grid(row=2, column=0)

        # ------ END OF CODE SECTION / START EXEC LOOP ------
        # The MainLoop
        self.rWin.mainloop()

    def initialize_apache(self):
        self.apache = ass.ApacheSmartSearch(self.url.get())
        url_success, code = self.apache.exist_url()
        if url_success:
            self.apache.generate_queries()
            self.set_TreeList()
            tk.messagebox.showinfo("Info", "Queries were generated successfully!")
        else:
            answer = tk.messagebox.showerror(f"Critical Error (Code {code})", "Server domain not found!")

    def setRootWin(self, title: str, resizable: bool, iconPath: str):
        self.rWin.title(title)
        try:
            self.rWin.iconphoto(False, tk.PhotoImage(file=iconPath))
        except:
            pass
        self.rWin.resizable(resizable, resizable)

    def set_TreeList(self, qfilter=None, f_type='name'):
        if self.apache:
            queries = self.apache.get_queries(qfilter=qfilter, f_type=f_type)
            self.trv.delete(*self.trv.get_children())
            for query in queries:
                self.trv.insert("", "end", values=(query['id'], query['name'], query['type'], query['in_folder'][-1]))

    def action_Search(self):
        qfilter = self.textKeywordSearch.get()
        f_type = self.textSearchBy.get()
        self.set_TreeList(qfilter=qfilter, f_type=f_type)

    def action_Download(self):
        if self.apache:
            item = self.trv.item(self.trv.focus())['values']
            if self.apache.download(item=item):
                messagebox.showinfo("Info", f"Item named \"{item[1]}\" downloaded successfully!")
            else:
                messagebox.showwarning("Warning", "An error occurred while downloading the item. Downloaded item may be wrong or corrupted!")


if __name__ == "__main__":
    main = ApacheSmartQueriesGUI()
